
classDiagram

class IProdutoCRUDUseCase~Produto~ {
            <<interface>>
            
            
        }
IGenericCRUDUseCase~T~<|..IProdutoCRUDUseCase~Produto~
class ProdutoCRUDUseCase{
            
            
        }
GenericCRUDUseCase~T~<|--ProdutoCRUDUseCase
IProdutoCRUDUseCase~Produto~<|..ProdutoCRUDUseCase
class GenericRepository~T~{
            #genericConnection: any
            +add() Promise~any~
+update() Promise~any~
+delete() Promise~any~
+getById() Promise~any~
+get() Promise~any~
        }
IGenericRepository~T~<|..GenericRepository~T~
class IGenericConnection~T~ {
            <<interface>>
            
            +post() Promise~any~
+get() Promise~any~
+update() Promise~any~
+getById() Promise~any~
+remove() Promise~any~
        }
class IGenericRepository~T~ {
            <<interface>>
            
            +add() Promise~any~
+update() Promise~any~
+delete() Promise~any~
+getById() Promise~any~
+get() Promise~any~
        }
class GenericCRUDUseCase~T~{
            -repository: any
-entity: string
            +getById() Promise~string~
+create() Promise~string~
+getAll() Promise~any~
+find() Promise~any~
+delete() Promise~string~
+update() Promise~string~
        }
IGenericCRUDUseCase~T~<|..GenericCRUDUseCase~T~
class IGenericCRUDUseCase~T~ {
            <<interface>>
            
            +create() Promise~string~
+getAll() Promise~string~
+find() Promise~string~
+getById() Promise~string~
+delete() Promise~string~
+update() Promise~string~
        }
class Categoria{
            +nome: string
            
        }
Entity<|--Categoria
class Entity{
            +id: number
            
        }
class Produto{
            +nome: string
+preco: number
+categoriaId: number
            
        }
Entity<|--Produto
class Field{
            +name: string
+label: string
+type: string
+rules: []
+defaultValue: any
+options: { value: string; text: string; }[]
            
        }
IField<|..Field
class IField {
            <<interface>>
            +name: string
+label: string
+type: string
+rules: any[]
+defaultValue: any
+options: { value: string; text: string; }[]
            
        }
class CategoriaRepository {
            <<interface>>
            
            
        }
IGenericRepository~T~<|..CategoriaRepository
class ProdutoRepository {
            <<interface>>
            
            
        }
IGenericRepository~T~<|..ProdutoRepository
class GenericConnection~T~{
            -path: string
-apiBaseUrl: string
-token?: string
            +post() Promise~any~
+get() Promise~any~
+update() Promise~any~
+getById() Promise~any~
+remove() Promise~any~
-createHeaders() { [key: string]: string; }
        }
IGenericConnection~T~<|..GenericConnection~T~
class ProdutoRepositoryImplment{
            
            
        }
GenericRepository~T~<|--ProdutoRepositoryImplment
ProdutoRepository<|..ProdutoRepositoryImplment

