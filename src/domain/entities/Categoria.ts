import { Entity } from "./Entity";

export class Categoria extends Entity {
  constructor(id: number, public nome: string) {
    super(id);
  }
}

export default Categoria;
