// Produto.ts
import { Entity } from './Entity';

export class Produto extends Entity {
  constructor(id: number, public nome: string, public preco: number, public categoriaId: number) {
    super(id);
  }
}

export default Produto;
