
import IRepositoryBase from '../../base/repository/IGenericRepository';
import { Categoria } from '../../domain/entities/Categoria'

export interface CategoriaRepository extends IRepositoryBase<Categoria> {
  
}
