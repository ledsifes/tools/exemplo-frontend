
import IRepositoryBase from '../../base/repository/IGenericRepository';
import { Produto } from '../entities/Produto';

export interface ProdutoRepository extends IRepositoryBase<Produto> {
  

}
