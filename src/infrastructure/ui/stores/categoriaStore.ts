// stores/produtoStore.ts
import { createEntityStore } from './factory/storeFactory';
import { Categoria } from '../../../domain/entities/Categoria';

const { useCase: categoriaUseCase, store: useCategoriaStore } = createEntityStore<Categoria>('categoria');

export { categoriaUseCase, useCategoriaStore };
