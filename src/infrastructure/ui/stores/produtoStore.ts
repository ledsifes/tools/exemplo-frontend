// stores/produtoStore.ts
import { createEntityStore } from './factory/storeFactory';
import { Produto } from '../../../domain/entities/Produto';

const { useCase: produtoUseCase, store: useProdutoStore } = createEntityStore<Produto>('produto');

export { produtoUseCase, useProdutoStore };
