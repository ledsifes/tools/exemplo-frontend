// stores/genericStore.ts
import { defineStore } from 'pinia';

export const createGenericStore = <T>(name: string, useCases: any) => {
  return defineStore(name, {
    state: () => ({
      items: [] as T[],
      filteredItems: [] as T[],
    }),
    actions: {
      async fetchItems(params: string) {
        this.items = await useCases.getAll(params);
        this.filteredItems = this.items;
      },
      async saveItem(item: T) {
        await  useCases.create(item);
        this.fetchItems('');
      },
      async updateItem(id: string, item: T) {
        //const newItem = FormToEntityAdapter.toEntity<T>(item);
        await  useCases.update(id, item);
        this.fetchItems('');
      },
      async deleteItem(id: string) {
        useCases.delete(id);
        this.fetchItems('');
      },
       filterItems(predicate: (item: T) => boolean) {//TODO: FIX THIS.
        this.filteredItems = this.items.filter(predicate);
      },
      resetFilter() {
        this.filteredItems = this.items;
      },
    },
  });
};
