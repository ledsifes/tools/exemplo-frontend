// factories/storeFactory.ts
import GenericRepository from '../../../../base/repository/GenericRepository';
import GenericUseCase from '../../../../base/usecase/GenericCRUDUseCase';
import GenericConnection from '../../../data/GenericConnection';
import { createGenericStore } from '../genericStore';

export function createEntityStore<T>(entityName: string) {
  const genericRepo = new GenericRepository(new GenericConnection(entityName))
  const useCase = new GenericUseCase<T>(entityName,genericRepo);
  const store = createGenericStore<T>(entityName, useCase);
  return { useCase, store };
}
