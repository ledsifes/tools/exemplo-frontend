import { ref, onMounted, watchEffect, Ref } from 'vue';

export const useEntityMixin = <T extends { id: string | number }>(store: any, defaultItem: () => T) => {
  const items = ref<T[]>([]);
  const dialog = ref(false);
  const itemEdit: Ref<T | null> = ref(null);
  const loading = ref(true);

  const openDialog = (item: T | null) => {
    itemEdit.value = item ? { ...item } : defaultItem();
    dialog.value = true;
  };

  const salvarItem = async (item: T) => {
    loading.value = true;
    if (item.id) {
      await store.updateItem(item.id.toString(), item);
    } else {
      item.id = Date.now().toString();
      await store.saveItem(item);
    }
    await store.fetchItems('');
    items.value = store.items;
    dialog.value = false;
    loading.value = false;
  };

  const removerItem = async (id: string) => {
    loading.value = true;
    await store.deleteItem(id);
    await store.fetchItems('');
    items.value = store.items;
    loading.value = false;
  };

  onMounted(async () => {
    await store.fetchItems('');
    items.value = store.items;
    loading.value = false;
  });

  watchEffect(() => {
    items.value = store.items;
  });

  return {
    items,
    dialog,
    itemEdit,
    openDialog,
    salvarItem,
    removerItem,
    loading,
  };
};
