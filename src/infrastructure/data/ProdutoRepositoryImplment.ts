import { ProdutoRepository } from '../../domain/repositories/ProdutoRepository'
import GenericRepository from '../../base/repository/GenericRepository'
import GenericConnection from './GenericConnection'
import { Produto } from '../../domain/entities/Produto';

//TODO: caso seja necessario especializar....
export class ProdutoRepositoryImplment extends GenericRepository<Produto> implements ProdutoRepository {
    
  constructor( ){
    super(new GenericConnection('produto'));
  }

}
