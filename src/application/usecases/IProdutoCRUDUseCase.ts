import IGenericUseCase from '../../base/usecase/IGenericCRUDUseCase';
import { Produto } from '../../domain/entities/Produto'

interface IProdutoCRUDUseCase<Produto> extends IGenericUseCase<Produto> {
    

}
export default IProdutoCRUDUseCase;