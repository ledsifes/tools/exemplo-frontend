import IProdutoCRUDUseCase from './IProdutoCRUDUseCase'
import Produto from '../../domain/entities/Produto'
import GenericCRUDUseCase from '../../base/usecase/GenericCRUDUseCase';
import { ProdutoRepositoryImplment } from '../../infrastructure/data/ProdutoRepositoryImplment';

class ProdutoCRUDUseCase extends GenericCRUDUseCase<Produto> implements IProdutoCRUDUseCase<Produto>{
  
    constructor() {
    super('produto' , new ProdutoRepositoryImplment());
  }

 
}
export default ProdutoCRUDUseCase;