import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './infrastructure/ui/App.vue'
import vuetify from './infrastructure/ui/plugins/vuetify'
import './infrastructure/ui/index.css'
import '@mdi/font/css/materialdesignicons.css'; // importação dos estilos de ícones



const app = createApp(App)
const pinia = createPinia()

app.use(pinia)
app.use(vuetify)
app.mount('#app')
