interface IGenericCRUDUseCase<T> {
    
    create(data: T): Promise<string>;
    getAll(params: any): Promise<string>;
    find(page: number, quant : number, filter: any): Promise<string>;
    getById(id: string): Promise<string>;
    delete(id: string): Promise<string>;
    update(id: string, data: T): Promise<string>;
  }
export default IGenericCRUDUseCase;