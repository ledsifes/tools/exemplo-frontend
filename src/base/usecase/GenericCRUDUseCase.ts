import IRepository from '../repository/IGenericRepository';
import IGenericCRUDUseCase from './IGenericCRUDUseCase';
//TODO: melhorar o retorno de erros em todas as funcoes


class GenericCRUDUseCase<T> implements IGenericCRUDUseCase<T>{
  private  repository; // Interface do repositório genérico
  private entity: string; 

  constructor(entity: string , repository: IRepository<T>) {
    this.repository = repository;
    this.entity = entity;
  }
  async getById(id: string): Promise<string> {
    try {
      const dataCreated = await this.repository.getById(id);
      return dataCreated;
    } catch (error: any) {
      throw new Error(`Failed getById ${this.entity}: ${error.message}`); 
    }
  }

  async create(entity: T): Promise<string> {
    try {
      const data = await this.repository.add(entity);
      return data;
    } catch (error: any) {
      throw new Error(`Failed create ${this.entity}: ${error.message}`);
    }
  }

  async getAll(params: any): Promise<any> {
    try {
      const data = await this.repository.get(params );
      return data;
    } catch (error: any) {
      throw new Error(`Failed getAll ${this.entity}: ${error.message}`);
    }
  }

  async find(page: number, quant : number, filter: any): Promise<any> {
    try {
      //build query for page and quant and filter. 
      const params = { 'page': page , 'quant' : quant , 'filter:': filter};
      const data = await this.repository.get(params);
      return data;
    } catch (error: any) {
      throw new Error(`Failed find ${this.entity}: ${error.message}`);
    }
  }

  async delete(id: string): Promise<string> {
    try {
      const data = await this.repository.delete(id);
      return data;
    } catch (error: any) {
      throw new Error(`Failed create ${this.entity}: ${error.message}`);
    }
  }


  async update(id: string, entity: T): Promise<string> {
    try {
      const data = await this.repository.update(id, entity);
      return data;
    } catch (error: any) {
      throw new Error(`Failed create ${this.entity}: ${error.message}`);
    }
  }

}

export default GenericCRUDUseCase;
