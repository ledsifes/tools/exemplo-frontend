/*@author: Felipe F. de Oliveira
*/

import GenericConnection from './IGenericConnection';
import IGenericRepository from './IGenericRepository';

class  GenericRepository<T> implements IGenericRepository<T> {

    protected genericConnection;

    constructor(genericConnection: GenericConnection<T>) {
        this.genericConnection = genericConnection;
    }
    async add(data: T) {
        const response = await this.genericConnection.post(data);
        return response;
    }
    async update( id: string, data: T) {
        if (id) {
            const response = await this.genericConnection.update( id , data,);
            return response;
        } else
            throw new Error('Identificador eh obrigatorio para a atualizacao desta entidade');
    }
    async delete(id: string) {
        const response = await this.genericConnection.remove(id);
        return response;
    }
    async getById(id: string) {
        let response;
        response = await this.genericConnection.getById(id);
        return response;
    }

    async get(params: any) {
        let response;
        response = await this.genericConnection.get(params);
        return response;
    }
}

export default GenericRepository;
