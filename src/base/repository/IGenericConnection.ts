interface IGenericConnection<T> {
    
    post(data: T, options?: { id?: string; }): Promise<any>;
    get(params: any): Promise<any>;
    update(id: string, data: T): Promise<any>;
    getById(id: string): Promise<any>;
    remove(id: string): Promise<any>;
  }
  
export default IGenericConnection;