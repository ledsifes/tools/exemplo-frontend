/* eslint-disable @typescript-eslint/no-explicit-any */
interface IGenericRepository<T> {
    add(data: T): Promise<any>;
    update( id: string , data: T): Promise<any>;
    delete(id: string): Promise<any>;
    getById(id: string): Promise<any>;
    get(params: any): Promise<any>;
}

export default IGenericRepository;