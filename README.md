# Introducao 
Este projeto é um exemplo de aplicação frontend utilizando Vue 3 com TypeScript, Vite e Pinia. Ele demonstra como configurar e utilizar essas tecnologias em conjunto para criar uma aplicação eficiente e escalável. Além disso, a aplicação utiliza um servidor JSON (json-server) para simular uma API backend para fins de teste e desenvolvimento.

# Stack

-Vue 3: Framework JavaScript progressivo para construção de interfaces de usuário.
-TypeScript: Superset de JavaScript que adiciona tipagem estática.
-Vite: Ferramenta de build rápida para desenvolvimento moderno de frontend.
-Pinia: Biblioteca de gerenciamento de estado para Vue, alternativa ao Vuex.
json-server: Ferramenta para criar um servidor RESTful a partir de um arquivo JSON.

-Estrutura do Projeto

-server/: Contém o json-server configurado para fornecer dados de exemplo.
-src/: Contém o código fonte da aplicação Vue.

-infraestrutura/: Store, view, Connection - Configuração e implementação do Pinia para gerenciamento de estado.

# Stack  

Vue 3 + TypeScript + Vite + pinia 

# Executando

## Rodando o servidor json server 
Este comando vai start o json server para testes locais 
```
cd server
npm run start 

```
## Para rodar a aplicacao 

```
npm run dev 

```

## Tela principal da aplicacao 

![Tela crud Exemplo](./tela_crud.png "Tela crud Exemplo")


# Documentacao 


## Visão geral 

![Visao geral](./visao_geral.png "visao geral")

## Domain

![Domain](./domain.png "Domain")


## Repository 
![Generic Repository](./generic_repository.png "Generic Repository")

## Use Case
![Generic Use case](./generic_usecase.png "Generic_usecase")

## Infra

### Connecion - Axios 
![Generic Connection](./generic_connection.png "Generic Connection")

### UI 

## Store 

## Connection


















